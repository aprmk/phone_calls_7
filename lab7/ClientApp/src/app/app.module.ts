import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { DxDataGridModule } from 'devextreme-angular';
import { AddressComponentComponent } from './address-component/address-component.component';
import { CityComponentComponent } from './city-component/city-component.component';
import { CallsComponentComponent } from './calls-component/calls-component.component';
import { PersonComponentComponent } from './person-component/person-component.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    AddressComponentComponent,
    CityComponentComponent,
    CallsComponentComponent,
    PersonComponentComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'address', component: AddressComponentComponent },
      { path: 'city', component: CityComponentComponent },
      { path: 'call', component: CallsComponentComponent },
      { path: 'person', component: PersonComponentComponent }
    ]),
    DxDataGridModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
