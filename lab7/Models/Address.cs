using System.Collections.Generic;

namespace lab7.Models
{
    public class Address
    {
        #region Public Properties

        public int id { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house_number { get; set; }


        #endregion Public Properties
    }
}