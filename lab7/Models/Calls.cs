using System;

namespace lab7.Models
{
    public class Calls
    {
        #region Public Properties

        public int id { get; set; }
        public DateTime date_of_calls { get; set; }
        public float duration { get; set; }

        #endregion Public Properties
    }
}