using System.Collections.Generic;

namespace lab7.Models
{
    public class Persons
    {
        #region Public Properties

        public int id { get; set; }
        public string first_name { get; set; }
        public string second_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }


        #endregion Public Properties
    }
}