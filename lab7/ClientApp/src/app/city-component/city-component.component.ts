import { Component } from '@angular/core';
import { Service } from './city-component.service';

@Component({
  selector: 'cityComponent',
  templateUrl: './city-component.component.html',
  styleUrls: ['./city-component.component.css'],
  providers: [Service]
})

export class CityComponentComponent {
  dataSource: any;

  constructor(service: Service) { 
    this.dataSource = service.getCities();

  }
}
