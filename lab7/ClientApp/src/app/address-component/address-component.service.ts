import { Injectable } from '@angular/core';
import * as AspNetData from "devextreme-aspnet-data-nojquery";

// Assign the URL of your actual data service to the variable below
const url:string = '/';
const dataSource:any = AspNetData.createStore({
      key: 'id', 
      loadUrl: url + 'api/Addresses/Get',
      insertUrl: url + 'api/Addresses/Post',
      updateUrl: url + 'api/Addresses/Put',
      deleteUrl: url + 'api/Addresses/Delete',
        onBeforeSend: function(method, ajaxOptions) {
          ajaxOptions.xhrFields = { withCredentials: true };
        }
      });



@Injectable()
export class Service { 
  getAddresses() { return dataSource; }

}
